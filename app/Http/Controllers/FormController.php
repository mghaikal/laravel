<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function home(){
        return view('home');
    }

    public function form(){
        return view('form');
    }

    public function selamat(Request $Request){
        return view('selamat');
    }

    public function selamat_post(Request $Request){
        $first = $Request["first"];
        $last = $Request["last"];
        return view('selamat', compact('first', 'last'));
    }
}
